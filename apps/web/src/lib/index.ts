// Mock your api

/**
 *
 * @returns Api to get all blogs
 */

export async function getStaticBlogs() {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library
  const res = await fetch("http://localhost:1337/api/blogs?populate=*");
  const posts = await res.json();
  return {
    props: {
      posts,
    },
  };
}

/**
 *
 * @returns Api to filter all blogs by category
 */

export async function getStaticBlogByCategory(category: string) {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library
  const res = await fetch(
    `http://localhost:1337/api/blogs?filters[categories][name][$eq]=${category}`
  );
  const posts = await res.json();
  return {
    props: {
      posts,
    },
  };
}

/**
 *
 * @returns Api to fetch all categories
 */

export async function getStaticAllCategory(category: string) {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library
  const res = await fetch(`http://localhost:1337/api/categories`);
  const posts = await res.json();
  return {
    props: {
      posts,
    },
  };
}

export function groupDataFn(prop: any) {
  const categorizedData: any = {};

  prop.forEach((blog: any) => {
    const categories = blog.attributes.categories.data;
    const categoryName = categories[0].attributes.name;

    if (!categorizedData[categoryName]) {
      categorizedData[categoryName] = [];
    }
    categorizedData[categoryName].push(blog);
  });

  return categorizedData;
}
