"use client";
import React, { useState } from "react";

export default function Faq() {
  const [open, setOpen] = useState([0, 1]);

  const handleOpen = (idx: number) => {
    setOpen((prevOpen) => {
      if (prevOpen.includes(idx)) {
        // If the index is already in the open array, remove it
        return prevOpen.filter((item) => item !== idx);
      } else {
        // If the index is not in the open array, add it
        return [...prevOpen, idx];
      }
    });
  };

  return (
    <section className="pt-[9rem]">
      <article className="grid gap-2 text-center">
        <h1
          data-animation="scroll-fade-in-y"
          className="text-3xl font-semibold opacity-0"
        >
          Frequently Asked Questions
        </h1>
        <p
          data-animation="scroll-fade-in-y"
          className="text-2xl text-primary-accent-2 text-opacity-80 opacity-0"
        >
          Here are a few related questions that people frequently ask.
        </p>
      </article>
      <section className="mx-auto grid max-w-5xl gap-4 pt-[3rem] 2xl:max-w-6xl">
        {data.map((item, idx) => (
          <article
            key={idx}
            className="grid gap-1 rounded-md bg-white bg-opacity-60 p-6"
          >
            <div className="flex justify-between">
              <h1 className="text-xl font-semibold text-primary-500">
                {item?.title}
              </h1>
              <button onClick={() => handleOpen(idx)}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className={`lucide lucide-chevron-down text-primary-500 ${
                    open.includes(idx) ? "rotate-180" : "rotate-0"
                  } flex items-center ring-black transition-all focus:ring`}
                >
                  <path d="m6 9 6 6 6-6" />
                </svg>
              </button>
            </div>
            <div
              className={`${
                open.includes(idx) ? "h-fit" : "h-0"
              } overflow-hidden`}
            >
              <p className="text-lg antialiased ">{item?.text}</p>
            </div>
          </article>
        ))}
      </section>
    </section>
  );
}

const data = [
  {
    title: "Question 1",
    text: "ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.",
  },
  {
    title: "Question 2",
    text: "ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.",
  },
  {
    title: "Question 3",
    text: "ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.",
  },
  {
    title: "Question 4",
    text: "ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.",
  },
  {
    title: "Question 5",
    text: "ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time. ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.ChatBawa is an automated chat machine that provides human-like interaction and a personalised experience in various languages. It uses natural language processing and machine learning to understand and respond to customer queries in real-time.v",
  },
];
