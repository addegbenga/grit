import Image from "next/image";
import { LinkButton } from "../shared/ui";

export default function Playstore() {
  return (
    <section className="gradient-container-2 mt-[18rem] flex items-center rounded-[2.5rem]  ">
      <figure className="relative -mt-[8rem] h-[37rem]  grow">
        <Image src="/images/mockup-double.svg" alt="" fill />
      </figure>

      <article className="flex w-[20%] grow flex-col gap-4  overflow-hidden  ">
        <h1
          data-animation="scroll-fade-in-y"
          className="text-[2.9rem] font-semibold  leading-[3.8rem] tracking-tight opacity-0 "
        >
          Shop For Groceries
          <br /> On The Reliance App
        </h1>
        <p
          data-animation="scroll-fade-in-y"
          className="max-w-2xl text-2xl opacity-0 "
        >
          For a nicer shopping experience, our App is faster and more detailed.
          Check it out now.
        </p>
        <article
          data-animation="scroll-fade-in-y"
          className="flex gap-5 opacity-0 "
        >
          <LinkButton size="mdWithIcon" className="gap-2 w-fit">
            <svg
              width="24"
              height="29"
              viewBox="0 0 24 29"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_66_557)">
                <path
                  d="M17.2485 0.5C15.5878 0.698806 14.0653 1.52353 12.9916 2.80605C12.4614 3.40606 12.0633 4.11094 11.8233 4.87487C11.5834 5.63879 11.5068 6.44467 11.5987 7.24011C12.4332 7.22126 13.2526 7.0143 13.9959 6.63466C14.7392 6.25502 15.3872 5.71249 15.8917 5.04752C16.4073 4.41687 16.7926 3.69023 17.0255 2.90965C17.2585 2.12908 17.3342 1.31006 17.2485 0.5Z"
                  fill="white"
                />
                <path
                  d="M23.1473 9.89396C22.5208 9.05469 21.716 8.3647 20.7909 7.87362C19.8659 7.38253 18.8435 7.10256 17.7973 7.05383C15.2864 7.05383 14.2238 8.25298 12.4795 8.25298C10.6808 8.25298 9.31378 7.05383 7.14241 7.05383C5.95324 7.11835 4.79818 7.47434 3.779 8.09041C2.75982 8.70649 1.90779 9.56376 1.29797 10.5867C-0.731351 13.7229 -0.379911 19.6264 2.90113 24.6563C4.07537 26.4551 5.64348 28.4779 7.69771 28.4955C9.52317 28.513 10.037 27.3249 12.51 27.3129C14.983 27.3009 15.4515 28.512 17.2733 28.4927C19.3257 28.477 20.9796 26.2355 22.1538 24.434C22.8632 23.3627 23.4686 22.226 23.9618 21.0395C22.8671 20.62 21.9146 19.8974 21.2156 18.9564C20.5165 18.0153 20.0999 16.8946 20.0145 15.7254C19.929 14.5563 20.1783 13.387 20.7331 12.3543C21.2879 11.3216 22.1253 10.4682 23.1473 9.89396Z"
                  fill="white"
                />
              </g>
              <defs>
                <clipPath id="clip0_66_557">
                  <rect
                    width="23.9165"
                    height="28"
                    fill="white"
                    transform="translate(0.041748 0.5)"
                  />
                </clipPath>
              </defs>
            </svg>
            <article>
              <p className="text-xs text-left">Get it on</p>
              <h3>App Store</h3>
            </article>
          </LinkButton>
          <LinkButton size="mdWithIcon" className="gap-2 w-fit">
            <svg
              width="26"
              height="29"
              viewBox="0 0 26 29"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_66_569)">
                <path
                  d="M1.04912 1.5304C0.711653 1.97796 0.530337 2.52387 0.532988 3.0844V25.9137C0.530777 26.4345 0.687026 26.9436 0.980988 27.3735L13.4027 14.4169L1.04912 1.5304Z"
                  fill="white"
                />
                <path
                  d="M14.5405 13.2307L18.5706 9.03069L4.40542 0.849092C4.1097 0.675749 3.78208 0.563767 3.44212 0.519829C3.10216 0.475891 2.75684 0.500898 2.42676 0.593358L14.5405 13.2307Z"
                  fill="white"
                />
                <path
                  d="M14.5395 15.6041L2.30347 28.3675C2.56448 28.4556 2.8381 28.5007 3.1136 28.5009C3.56718 28.4999 4.01245 28.3792 4.4044 28.1509L18.6723 19.9161L14.5395 15.6041Z"
                  fill="white"
                />
                <path
                  d="M24.1752 12.2638L20.034 9.87256L15.6772 14.417L20.1358 19.0678L24.1752 16.7344C24.5675 16.5078 24.8932 16.1819 25.1197 15.7895C25.3461 15.3972 25.4654 14.9521 25.4654 14.4991C25.4654 14.0461 25.3461 13.601 25.1197 13.2086C24.8932 12.8163 24.5675 12.4904 24.1752 12.2638Z"
                  fill="white"
                />
              </g>
              <defs>
                <clipPath id="clip0_66_569">
                  <rect
                    width="24.934"
                    height="28"
                    fill="white"
                    transform="translate(0.532959 0.5)"
                  />
                </clipPath>
              </defs>
            </svg>
            <article>
              <p className="text-xs text-left">Download it on</p>
              <h3>Play Store</h3>
            </article>
          </LinkButton>
        </article>
      </article>
    </section>
  );
}
