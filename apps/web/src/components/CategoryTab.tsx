"use client";
import { LinkButton } from "@/shared/ui";
import { ICategoryReturnType } from "@/types";
import { useParams, useSearchParams } from "next/navigation";
import React from "react";

export default function CategoryTab({ data }: { data: ICategoryReturnType }) {
  const params = useParams();
  const url = (d: string) => (d === "All" ? "/blog" : `/blog/${d}`);
  return (
    <article className="grid gap-2 pt-4">
      <h1 className="text-lg">Category</h1>
      <section className="flex gap-2">
        {data.data.map((item, idx) => (
          <LinkButton
            href={url(item.attributes.name)}
            intent={
              params.slug && params.slug === item.attributes.name
                ? "primary"
                : !params.slug && item.attributes.name === "All"
                ? "primary"
                : "secondary"
            }
            size="sm"
            shape="sm"
            key={idx}
          >
            {item.attributes?.name}
          </LinkButton>
        ))}
      </section>
    </article>
  );
}
