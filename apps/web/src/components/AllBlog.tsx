import Image from "next/image";
import { IBlogReturnType, IBlogReturnTypeObj } from "../types";
import PostOfTheWeek from "./PostOfTheWeek";
import Link from "next/link";
export default function BlogView({ prop }: { prop: IBlogReturnType }) {
  return (
    <section>
      <PostOfTheWeek />
      {Object.keys(prop).map((item, idx) => (
        <section key={idx} className="grid gap-3 pt-[3rem] ">
          <article>
            <h1 className=" text-[2.2rem] font-medium leading-[5rem] tracking-tighter text-primary-accent-1">
              {decodeURI(item)}
            </h1>
          </article>
          <section className="flex gap-8 overflow-x-auto overflow-y-hidden ">
            {prop[item].map((blogitem: IBlogReturnTypeObj) => (
              <Link
                href={`/blog/${blogitem.attributes.blogTitle}/${blogitem.id}`}
                key={blogitem.id}
                className=" flex h-[23rem] min-w-[25rem] flex-col "
              >
                <section className="relative h-full w-full overflow-hidden rounded-[1.875rem]">
                  {blogitem?.attributes.url ? (
                    <Image
                      className="object-cover object-top"
                      // @ts-ignore
                      src={blogitem?.attributes?.url}
                      alt=""
                      fill
                    />
                  ) : (
                    ""
                  )}
                  <div className="absolute z-20 flex flex-col gap-2 bottom-2 left-5">
                    <span className="p-2 px-3 text-sm bg-white rounded-full ">
                      July 21st, 2023
                    </span>
                  </div>
                </section>
                <article className="mt-2">
                  <h1 className="text-xl text-primary-accent-2">
                    {blogitem.attributes.blogTitle}
                  </h1>
                  <p className="text-primary-accent-2 text-opacity-70">
                    by {blogitem.attributes.author}
                  </p>
                </article>
              </Link>
            ))}
          </section>
        </section>
      ))}
    </section>
  );
}
