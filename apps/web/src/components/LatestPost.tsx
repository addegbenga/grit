import Image from "next/image";
import React from "react";
export default function LatestPost() {
  return (
    <section className="grid gap-4 pt-[7rem]">
      <article>
        <h1 className=" text-[2.7rem] font-bold leading-[5rem] tracking-tighter text-primary-accent-1">
          Our Latest Post
        </h1>
      </article>
      <section className="flex h-[28rem]  gap-8 overflow-hidden ">
        <Cards />
        <section className="grid w-[15%] grow gap-8">
          <Card2 />
          <Card3 />
        </section>
      </section>
    </section>
  );
}

function Cards() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative flex h-full grow items-end overflow-hidden rounded-[2.5rem] opacity-0"
    >
      <div className="absolute left-5 top-5 z-20 flex flex-col gap-2">
        <span className="rounded-full bg-white p-2 px-3 text-sm ">
          July 21st, 2023
        </span>
        <span className="w-fit rounded-full border border-white bg-white bg-opacity-5 p-1 px-2 text-sm text-white ">
          Health
        </span>
      </div>
      <figure className="absolute left-0 top-0 h-full w-full">
        <Image
          className="object-cover object-top"
          src="/images/grocery.jpg"
          alt=""
          fill
        />
      </figure>
      <article className="relative z-20 m-5 flex flex-col rounded-2xl bg-white p-2 px-4">
        <h1 className="text-2xl font-medium ">
          How to know the best fruit too pick from
        </h1>
        <p className="text-primary-accent-2 text-opacity-60 ">
          by Olaniyi Bamidele
        </p>
      </article>
    </section>
  );
}

function Card3() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative flex h-full grow items-end overflow-hidden rounded-[2.5rem] opacity-0"
    >
      <div className="absolute left-5 top-5 z-20 flex flex-col gap-2">
        <span className="rounded-full bg-white p-2 px-3 text-sm ">
          July 21st, 2023
        </span>
        <span className="w-fit rounded-full border border-white bg-[#1b1b1b4d] bg-opacity-5 p-1 px-2 text-sm text-white ">
          Health
        </span>
      </div>
      <figure className="absolute left-0 top-0 h-full w-full">
        <Image
          className="object-cover object-top"
          src="/images/melon.jpg"
          alt=""
          fill
        />
      </figure>
      <article className="relative z-20 m-5 flex flex-col rounded-xl bg-white p-2 px-4">
        <h1 className="text-2xl font-medium ">Watermelon and its benefits</h1>
        <p className="text-primary-accent-2 text-opacity-60 ">
          by Olaniyi Bamidele
        </p>
      </article>
    </section>
  );
}
function Card2() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative flex h-full w-full items-end rounded-[2.5rem] bg-[#448E88] opacity-0 "
    >
      <div className="absolute left-5 top-5 z-20 flex flex-col gap-2">
        <span className="rounded-full bg-white p-2 px-3 text-sm ">
          July 21st, 2023
        </span>
        <span className="w-fit rounded-full border border-white bg-[#1b1b1b4d] bg-opacity-5 p-1 px-2 text-sm text-white ">
          Health
        </span>
      </div>
      <article className="relative z-20 m-5 flex flex-col rounded-xl p-2 px-4 text-white">
        <h1 className="text-2xl font-medium ">Watermelon and its benefits</h1>
        <p className=" text-opacity-60">by Olaniyi Bamidele</p>
      </article>
    </section>
  );
}
