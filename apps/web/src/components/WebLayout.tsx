import LandingFooter from "./Footer";
import LandingNavbar from "./Navbar";

type IProps = {
  children: React.ReactNode;
};
export default function WebLayout({ children }: IProps) {
  return (
    <main className="hidden w-full min-h-screen overflow-hidden lg:block bg-gradient-primary">
      <LandingNavbar />
      <section className="container mx-auto ">{children}</section>
      <LandingFooter />
    </main>
  );
}
