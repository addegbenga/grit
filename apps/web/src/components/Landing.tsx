import Choose from "./Choose";
import Consumptions from "./Consumptions";
import Flexible from "./Flexible";
import Hero from "./Hero";
import LatestPost from "./LatestPost";
import Partners from "./Partners";
import Playstore from "./Playstore";
import Reviews from "./Reviews";
import Faq from "./faq";

export function Landing() {
  return (
    <>
      <Hero />
      <Consumptions />
      <Choose />
      <LatestPost />
      <Playstore />
      <Partners />
      <Faq />
      <Flexible />
      <Reviews />
    </>
  );
}
