"use client";

import React from "react";
import Image from "next/image";
import { useBlogAnimation } from "../shared/animations";
export default function PostOfTheWeek() {
  useBlogAnimation();
  return (
    <section className="grid gap-4 pt-[3rem] ">
      <article>
        <h1 className=" text-[2.2rem] font-bold leading-[5rem] tracking-tighter text-primary-accent-1">
          Post of the week
        </h1>
      </article>
      <section className="flex h-[28rem]  gap-8 overflow-hidden ">
        <Cards />
        <section className="grid w-[18%] grow gap-8">
          <Card2 />
          <Card3 />
        </section>
      </section>
    </section>
  );
}

function Cards() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative opacity-0 flex h-full grow items-end overflow-hidden rounded-[1.875rem] "
    >
      <div className="absolute z-20 flex flex-col gap-2 left-5 top-5">
        <span className="p-2 px-3 text-sm bg-white rounded-full ">
          July 21st, 2023
        </span>
        <span className="p-1 px-2 text-sm text-white bg-white border border-white rounded-full w-fit bg-opacity-5 ">
          Health
        </span>
      </div>
      <figure className="absolute top-0 left-0 w-full h-full transition-transform ease-in transform duration-400 hover:scale-105">
        <Image
          className="object-cover object-top"
          src="/images/grocery.jpg"
          alt=""
          fill
        />
      </figure>
      <article className="relative z-20 flex flex-col p-2 px-4 m-5 bg-white rounded-2xl">
        <h1 className="text-2xl font-medium ">
          How to know the best fruit too pick from
        </h1>
        <p className="text-primary-accent-2 text-opacity-60 ">
          by Olaniyi Bamidele
        </p>
      </article>
    </section>
  );
}

function Card3() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative opacity-0 flex h-full grow items-end overflow-hidden rounded-[1.875rem]"
    >
      <div className="absolute z-20 flex flex-col gap-2 left-5 top-5">
        <span className="p-2 px-3 text-sm bg-white rounded-full ">
          July 21st, 2023
        </span>
        <span className="w-fit rounded-full border border-white bg-[#1b1b1b4d] bg-opacity-5 p-1 px-2 text-sm text-white ">
          Health
        </span>
      </div>
      <figure className="absolute top-0 left-0 w-full h-full">
        <Image
          className="object-cover object-top"
          src="/images/melon.jpg"
          alt=""
          fill
        />
      </figure>
      <article className="relative z-20 flex flex-col p-2 px-4 m-5 bg-white rounded-xl">
        <h1 className="text-2xl font-medium ">Watermelon and its benefits</h1>
        <p className="text-primary-accent-2 text-opacity-60 ">
          by Olaniyi Bamidele
        </p>
      </article>
    </section>
  );
}
function Card2() {
  return (
    <section
      data-animation="trigger-fade-in-y"
      className="relative opacity-0 flex h-full w-full items-end rounded-[1.875rem] bg-[#448E88]  "
    >
      <div className="absolute z-20 flex flex-col gap-2 left-5 top-5">
        <span className="p-2 px-3 text-sm bg-white rounded-full ">
          July 21st, 2023
        </span>
        <span className="w-fit rounded-full border border-white bg-[#1b1b1b4d] bg-opacity-5 p-1 px-2 text-sm text-white ">
          Health
        </span>
      </div>
      <article className="relative z-20 flex flex-col p-2 px-4 m-5 text-white rounded-xl">
        <h1 className="text-2xl font-medium ">Watermelon and its benefits</h1>
        <p className=" text-opacity-60">by Olaniyi Bamidele</p>
      </article>
    </section>
  );
}
