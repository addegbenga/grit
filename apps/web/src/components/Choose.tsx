import Image from "next/image"
import Link from "next/link"

export default function Choose() {
  return (
    <section className="pt-[10rem]">
      <article>
        <h1 className=" text-[2.7rem] font-bold leading-[5rem] tracking-tight text-primary-accent-1">
          Why Choose Relianceshopper?
        </h1>
      </article>
      <section className="flex justify-center pt-[3rem]  2xl:gap-10">
        <figure className="relative w-[35rem] 2xl:w-[25rem]  ">
          <Image src="/images/mockup1.svg" className="object-contain" alt="" fill />
        </figure>
        <section className="my-7 grid gap-4 ">
          {data.map((item, idx) => (
            <article
              data-animation="trigger-fade-in-y"
              key={idx}
              className="grid gap-2 rounded-md bg-gradient-primary-3 p-5 opacity-0 2xl:p-7 "
            >
              <h1 className="text-2xl font-semibold tracking-tight">{item.title}</h1>
              <p className="text-lg ">{item.text}</p>
              <Link href="/" className="text-primary-500">
                Read More
              </Link>
            </article>
          ))}
        </section>
      </section>
    </section>
  )
}

const data = [
  {
    title: "Fast & Timely Delivery",
    text: "Time is a very precious commodity for this reason, we do not delay. we ensure prompt delivery of orders to customers.",
  },
  {
    title: "Healthy Groceries",
    text: "We prioritize healthy groceries sourced from a reputable suppliers. This enables us to provide our customers with superior products.",
  },
  {
    title: "Earn as a Shopper",
    text: "Reliance cart is an avenue where you get paid by shopping for others. As a shopper, you are indirectly hired by people to deliver their selected items.",
  },
]
