import Image from "next/image";
import React from "react";

export default function Partners() {
  return (
    <section className="pt-[10rem]">
      <article className="grid gap-2 text-center">
        <h1
          data-animation="scroll-fade-in-y"
          className="text-[2.8rem] font-semibold tracking-tight text-primary-accent-1 opacity-0"
        >
          Our Partners
        </h1>
        <p
          data-animation="scroll-fade-in-y"
          className="mx-auto max-w-3xl text-2xl text-primary-accent-2 text-opacity-60 opacity-0"
        >
          We work with like-minded grocery ventures that provide items that suit
          our intented goals
        </p>
      </article>
      <section className="grid grid-cols-4 gap-4 pt-[2rem]">
        {data.map((item, idx) => (
          <Card key={idx} item={item} />
        ))}
      </section>
    </section>
  );
}

function Card({ ...props }: { item: { icon: string } }) {
  return (
    <article className="rounded-[1.5rem] bg-white bg-opacity-80 p-5">
      <figure className="relative h-[3rem]  ">
        <Image src={props?.item.icon} alt="" fill />
      </figure>
    </article>
  );
}

const data = [
  {
    icon: "/brands/walmart.svg",
  },
  {
    icon: "/brands/simi.svg",
  },
  {
    icon: "/brands/nofrills.svg",
  },
  {
    icon: "/brands/walmart.svg",
  },
  {
    icon: "/brands/simi.svg",
  },
  {
    icon: "/brands/walmart.svg",
  },
  {
    icon: "/brands/nofrills.svg",
  },
  {
    icon: "/brands/simi.svg",
  },
];
