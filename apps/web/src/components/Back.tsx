"use client";
import { useRouter } from "next/navigation";
import React from "react";

export function BackBtn({ children }: { children: React.ReactNode }) {
  const router = useRouter();
  return <button onClick={() => router.back()}>{children}</button>;
}
