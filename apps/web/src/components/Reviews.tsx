import { StarFilledIcon } from "@radix-ui/react-icons";
import Image from "next/image";

export default function Reviews() {
  return (
    <section className="pt-[7rem]">
      <article className="text-center">
        <h1 className="text-4xl font-semibold tracking-tight text-primary-accent-1">
          What our clients are saying
        </h1>
      </article>
      <section className="relative mx-auto mt-[3rem] max-w-5xl 2xl:max-w-6xl">
        <section className="grid gap-5 lg:grid-cols-3 ">
          {reviewData.map((item, idx) => (
            <ReviewCard item={item} key={idx + "dhhb"} />
          ))}
        </section>
      </section>
    </section>
  );
}

interface IProps {
  item: {
    name: string;
    url: string;
    role: string;
    text: string;
  };
}

export const ReviewCard = ({ item }: IProps) => {
  return (
    <div className="flex h-full w-fit flex-col justify-between rounded-[20px] border bg-white bg-opacity-50  p-7 lg:rounded-[30px]">
      <div className="">
        <div className="flex gap-0.5">
          {[1, 2, 3, 4, 5].map((item, idx) => (
            <StarFilledIcon key={idx} className="h-4 w-4 text-[#F49D10]" />
          ))}
        </div>
        <p className="mt-4 mb-7">{item.text}</p>
      </div>
      <div className="flex items-center gap-3 mt-auto">
        <Image src={item.url} alt="" width={43} height={43} />
        <div>
          <h2 className="text-lg font-bold ">{item.name}</h2>
          <p className="font-light truncate ">{item.role}</p>
        </div>
      </div>
    </div>
  );
};

const reviewData = [
  {
    name: "Jenny Wilson",
    url: "/images/avatar1.svg",
    role: "CEO",
    text: "Relianceshopper has revolutionized currency management for our business. I highly recomment it’s user-friendly interface.",
  },
  {
    name: "Robert Fox",
    url: "/images/avatar2.svg",
    role: "CFO",
    text: "As a CFO, Relianceshopper has been an invaluable tool for secure transactions. It's advances security measures.",
  },
  {
    name: "Cody Fisher",
    url: "/images/avatar3.svg",
    role: "Travel Coordinator",
    text: "Being a travel coordinator, Relianceshopper has simplified my job immensely. I can always count on it. ",
  },
];
