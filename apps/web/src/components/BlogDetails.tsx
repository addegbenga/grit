import React from "react";
import Image from "next/image";
import { IBlogReturnTypeObj } from "@/types";
export default function BlogDetails({
  data,
}: {
  data: {
    meta: any;
    data: IBlogReturnTypeObj;
  };
}) {
  return (
    <section>
      <h1 className="text-[2rem] my-2 tracking-tight font-semibold">
        {data.data.attributes.blogTitle}
      </h1>
      <article className="flex items-center gap-1 text-sm">
        <div className="text-[#1D5506] flex divide-x-2 ">
          {data.data.attributes.categories.data.map((item) => (
            <span className="px-2" key={item.id}>
              {item.attributes.name}
            </span>
          ))}
        </div>
        <span className="px-2  text-[#939495]">November 6, 2020</span>
      </article>
      <section className="pt-[2rem]">
        <figure className="w-full relative rounded-[1.5rem] h-[28rem] overflow-hidden">
          <Image
            //   @ts-ignore
            src={data.data.attributes.url}
            className="object-cover"
            alt=""
            fill
          />
        </figure>
      </section>
    </section>
  );
}
