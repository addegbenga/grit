import Image from "next/image";
import { LinkButton } from "../shared/ui";

export default function Flexible() {
  return (
    <section className="mx-auto flex max-w-5xl items-center gap-10 pt-[5rem] 2xl:max-w-6xl">
      <article className="grid h-fit gap-4 overflow-hidden">
        <h1
          data-animation="scroll-fade-in-y"
          className="text-[2.5rem] font-semibold leading-[3.3rem] tracking-tight text-primary-accent-1 opacity-0"
        >
          Are You Looking For A<br /> Flexible Part-Time Job?
        </h1>
        <p
          data-animation="scroll-fade-in-y"
          className="max-w-6xl text-xl text-primary-accent-2 text-opacity-70 opacity-0"
        >
          We provide an oppoturnity to earn extra income by completing the
          simple task of delivering items in a respectful and timely manner. You
          can seize this opporturnity to put more money into your pocket.
        </p>
        <LinkButton
          data-animation="scroll-fade-in-y"
          className="w-fit opacity-0 "
        >
          Start Shopping
        </LinkButton>
      </article>
      <figure className="relative h-[30rem] w-full ">
        <Image src="/images/Union.png" className="object-contain" alt="" fill />
      </figure>
    </section>
  );
}
