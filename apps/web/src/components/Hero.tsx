"use client";
import Image from "next/image";
import { LinkButton } from "../shared/ui";
import { useHeroAnimation } from "../shared/animations";

export default function Hero() {
  useHeroAnimation();
  return (
    <section className="mx-auto  flex items-center justify-between pt-[4rem] 2xl:pt-[6rem]">
      <section className="grid h-fit shrink-0 gap-8">
        <article className="grid gap-2 overflow-hidden ">
          <h1
            data-animation="fade-in-y"
            className="text-[3.7rem] font-black leading-[4.2rem] tracking-tight text-primary-accent-1 opacity-0"
          >
            Health Grocery <br /> Delivery and Foods
          </h1>
          <p
            data-animation="fade-in-y"
            className="text-[1.35rem] font-normal leading-[2.09375rem]  tracking-[-0.04375rem] text-primary-accent-2 antialiased opacity-0"
          >
            Your health gets better with our nutrition and delivery
            <br /> services. We go beyond serving you as we are meticulous
            <br /> about your well being.
          </p>
        </article>
        <article
          data-animation="fade-in-y"
          className="flex gap-[1.5rem] opacity-0"
        >
          <LinkButton>Download</LinkButton>
          <LinkButton intent="outline-primary">Become a Partner</LinkButton>
        </article>
      </section>
      <section
        data-animation="fade-in"
        className="relative flex h-[34rem] w-full opacity-0 "
      >
        <figure className="relative z-10 h-full w-full ">
          <Image fill src="/images/basket.svg" alt="A beautiful landscape" />
        </figure>
        <figure className="absolute -top-[5rem] left-[14rem] h-[80rem] w-full 2xl:-top-[5rem]">
          <Image fill src="/images/curve.svg" alt="" />
        </figure>
      </section>
    </section>
  );
}
