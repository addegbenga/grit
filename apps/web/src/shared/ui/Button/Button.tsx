import { cva, type VariantProps } from "class-variance-authority";
import Link from "next/link";
import { twMerge } from "tailwind-merge";

const button = cva(
  [
    "justify-center",
    "inline-flex",
    "items-center",
    "text-center",
    "transition-colors",
    "delay-50",
  ],
  {
    variants: {
      intent: {
        primary: ["bg-primary-500", "text-white", "hover:enabled:bg-primary-"],
        secondary: ["bg-[rgba(19,172,119,0.10)]", "text-primary-500"],
        "outline-primary": [
          "border-primary-500 border-2 bg-transparent text-primary-500",
        ],
      },
      size: {
        sm: [
          "min-w-20",
          "h-full",
          "min-h-10",
          "text-[1rem]",
          "py-[0.7rem]",
          "px-[2.5rem]",
        ],
        md: [
          "min-w-20",
          "h-full",
          "min-h-10",
          "text-[1.25rem]",
          "py-[1.3rem]",
          "px-[3.25rem]",
        ],
        mdWithIcon: [
          "min-w-20",
          "h-full",
          "min-h-10",
          "text-[1.25rem]",
          "py-[1.3rem]",
          "px-[2rem]",
        ],
      },
      underline: { true: ["underline"], false: [] },
      shape: {
        sm: "rounded-[0.8rem]",
        md: "rounded-[1.5rem]",
        full: "rounded-full",
      },
    },
    defaultVariants: {
      intent: "primary",
      size: "md",
      shape: "md",
    },
  }
);

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLAnchorElement>,
    VariantProps<typeof button> {
  underline?: boolean;
  href?: string;
}

export function LinkButton({
  className,
  href = "/",
  intent,
  size,
  underline,
  shape,
  ...props
}: ButtonProps) {
  return (
    <Link
      href={href}
      className={twMerge(button({ intent, size, className, underline, shape }))}
      {...props}
    >
      {props.children}
    </Link>
  );
}
export interface ButtonsProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    VariantProps<typeof button> {}

export const Button: React.FC<ButtonsProps> = ({
  className,
  intent,
  size,
  ...props
}) => <button className={button({ intent, size, className })} {...props} />;
