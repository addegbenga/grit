import { useEffect } from "react";
import gsap, { Power2 } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);
export const useHeroAnimation = () => {
  //First animation on the hero section runs only once
  useEffect(() => {
    const timeline = gsap.timeline();
    timeline.fromTo(
      '[data-animation="fade-in-y"]',
      { yPercent: 70, opacity: 0, duration: 0.5 },
      {
        yPercent: 0,
        opacity: 1,
        ease: Power2.easeOut,
        stagger: {
          amount: 0.2,
        },
      },
      0.1
    );
    timeline.to(
      '[data-animation="fade-in"]',
      {
        opacity: 1,
        ease: Power2.easeIn,
      },
      "-=0.5"
    );
  }, []);

  useEffect(() => {
    gsap.defaults({ ease: "power3" });
    gsap.set('[data-animation="trigger-fade-in-y"]', { y: 50 });
    ScrollTrigger.batch('[data-animation="trigger-fade-in-y"]', {
      onEnter: (batch) =>
        gsap.to(batch, {
          opacity: 1,
          y: 0,
          stagger: { each: 0.15, grid: [1, 3] },
          overwrite: true,
        }),
      onLeave: (batch) =>
        gsap.set(batch, { opacity: 0, y: -50, overwrite: true }),
      onEnterBack: (batch) =>
        gsap.to(batch, { opacity: 1, y: 0, stagger: 0.15, overwrite: true }),
      onLeaveBack: (batch) =>
        gsap.set(batch, { opacity: 0, y: 50, overwrite: true }),
      // you can also define things like start, end, etc.
    });
    ScrollTrigger.batch('[data-animation="scroll-fade-in-y"]', {
      onEnter: (batch) =>
        gsap.fromTo(
          batch,
          { yPercent: 70, opacity: 0, duration: 0.5 },
          {
            yPercent: 0,
            opacity: 1,
            ease: Power2.easeOut,
            stagger: {
              amount: 0.2,
            },
          }
        ),
    });
  }, []);
};

export const useBlogAnimation = () => {
  useEffect(() => {
    gsap.defaults({ ease: "power3" });
    gsap.set('[data-animation="trigger-fade-in-y"]', { y: 50 });
    ScrollTrigger.batch('[data-animation="trigger-fade-in-y"]', {
      onEnter: (batch) =>
        gsap.to(batch, {
          opacity: 1,
          y: 0,
          stagger: { each: 0.15, grid: [1, 3] },
          overwrite: true,
        }),
      onLeave: (batch) =>
        gsap.set(batch, { opacity: 0, y: -50, overwrite: true }),
      onEnterBack: (batch) =>
        gsap.to(batch, { opacity: 1, y: 0, stagger: 0.15, overwrite: true }),
      onLeaveBack: (batch) =>
        gsap.set(batch, { opacity: 0, y: 50, overwrite: true }),
      // you can also define things like start, end, etc.
    });
    ScrollTrigger.batch('[data-animation="scroll-fade-in-y"]', {
      onEnter: (batch) =>
        gsap.fromTo(
          batch,
          { yPercent: 70, opacity: 0, duration: 0.5 },
          {
            yPercent: 0,
            opacity: 1,
            ease: Power2.easeOut,
            stagger: {
              amount: 0.2,
            },
          }
        ),
    });
  }, []);
};
