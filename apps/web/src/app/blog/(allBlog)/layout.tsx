// These styles apply to every route in the application
import CategoryTab from "@/components/CategoryTab";
import { apiBase } from "@/services/constant";
import { ICategoryReturnType } from "@/types";

type IProps = {
  children: React.ReactNode;
};

export default async function BlogLayout({ ...props }: IProps) {
  const data = (await getCategoryData()) as ICategoryReturnType;

  const url = (d: string) => (d === "All" ? "/blog" : `/blog/${d}`);

  return (
    <main className="">
      <section className=" max-w-6xl mx-auto lg:pt-[3rem] 2xl:pt-[4rem]">
        <article className="grid gap-5 pb-10 border-b border-opacity-30 border-primary-500">
          <div className="flex items-center justify-between overflow-hidden">
            <h1 className="text-6xl font-bold text-primary-accent-1">Blog</h1>
          </div>
          <p className="max-w-xl text-lg tracking-tight text-primary-accent-2 text-opacity-90">
            Our articles are very informative, educative and easy to uderstand
            because we believe that the articles carter for various types of
            investor knowledge.
          </p>
        </article>
        {/* <article className="grid gap-2 pt-4">
          <h1 className="text-lg">Category</h1>
          <section className="flex gap-2">
            {data.data.map((item, idx) => (
              <LinkButton
                href={url(item.attributes.name)}
                intent={1 === idx ? "primary" : "secondary"}
                size="sm"
                shape="sm"
                key={idx}
              >
                {item.attributes?.name}
              </LinkButton>
            ))}
          </section>
        </article> */}
        <CategoryTab data={data} />
      </section>
      <section className="max-w-6xl mx-auto ">{props.children}</section>
    </main>
  );
}

async function getCategoryData() {
  const res = await fetch(`${apiBase}/api/categories`);

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}
