import React from "react";
import { IBlogReturnType } from "../../../types";
import { apiBase } from "@/services/constant";
import BlogView from "@/components/AllBlog";

export default async function Page() {
  const data = (await getData()) as IBlogReturnType;

  return <BlogView prop={data} />;
}

async function getData() {
  const res = await fetch(`${apiBase}/api/blogs?populate=*`);
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  const data = (await res.json()) as IBlogReturnType;
  const categorizedData: any = {};
  if (data.data.length > 0) {
    data.data.forEach((blog) => {
      const categories = blog.attributes.categories?.data;
      const categoryName = categories?.[0]?.attributes?.name;

      if (categoryName) {
        if (!categorizedData[categoryName]) {
          categorizedData[categoryName] = [];
        }
        categorizedData[categoryName].push(blog);
      }
    });
  }

  return categorizedData;
}
