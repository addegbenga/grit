import React from "react";
import FilterdBlogView from "../../../../components/FilteredBlog";
import { ICategoryReturnType, IBlogReturnType } from "../../../../types";
import { apiBase } from "@/services/constant";

export async function generateStaticParams() {
  const posts = (await fetch(`${apiBase}/api/categories`).then((res) =>
    res.json()
  )) as ICategoryReturnType;

  return posts.data.map((post) => ({
    slug: post.attributes.name,
  }));
}

export default async function Blog({ params }) {
  const { slug } = params;
  const data = (await getData(slug)) as IBlogReturnType;

  return <FilterdBlogView prop={data} categoryName={slug} />;
}

/**
 *
 * @param category
 * @returns This endpoints groups the blogs into category
 */
async function getData(category: string) {
  const res = await fetch(
    `${apiBase}/api/blogs?filters[categories][name][$eq]=${category}`
  );
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }
  return res.json();
}
