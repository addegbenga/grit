import React from "react";
import BlogDetails from "@/components/BlogDetails";
import { IBlogReturnTypeObj } from "@/types";
import { apiBase } from "@/services/constant";

export default async function page({ params }) {
  const data = (await getData(params.details)) as {
    meta: any;
    data: IBlogReturnTypeObj;
  };

  return <BlogDetails data={data} />;
}

async function getData(id: string) {
  const res = await fetch(`${apiBase}/api/blogs/${id}?populate=*`);
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }
  return res.json();
}
