import { BackBtn } from "@/components/Back";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <main className="max-w-4xl pt-[3rem] mx-auto">
      <BackBtn>
        <section className="flex items-center gap-2">
          <div className="flex items-center justify-center w-10 h-10 text-white rounded-full bg-primary-500">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="lucide lucide-arrow-left"
            >
              <path d="m12 19-7-7 7-7" />
              <path d="M19 12H5" />
            </svg>
          </div>
          <span className="text-primary-1">Back</span>
        </section>
      </BackBtn>
      {children}
    </main>
  );
}
