// These styles apply to every route in the application
import "../styles/globals.css";
import WebLayout from "../components/WebLayout";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <WebLayout>{children}</WebLayout>
      </body>
    </html>
  );
}
