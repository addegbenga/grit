export interface IBlogReturnType {
  data: IBlogReturnTypeObj[];
  meta: Meta;
}
export interface ICategoryReturnType {
  data: CategoriesDatum[];
  meta: Meta;
}

export interface IBlogReturnTypeObj {
  id: number;
  attributes: PurpleAttributes;
}

export interface PurpleAttributes {
  blogTitle: string;
  author: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  content: string;
  url: ThumbnailReturnType;
  categories: Categories;
}

export interface Categories {
  data: CategoriesDatum[];
}

export interface CategoriesDatum {
  id: number;
  attributes: CategoriesAttributes;
}

export interface CategoriesAttributes {
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  name: string;
}

export interface ThumbnailReturnType {
  data: ThumbnailDataAttributesObj;
}

export interface ThumbnailDataAttributesObj {
  id: number;
  attributes: ThumbnailDataAttributes;
}

export interface ThumbnailDataAttributes {
  name: string;
  alternativeText: null;
  caption: null;
  width: number;
  height: number;
  formats: Formats;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: null;
  provider: string;
  provider_metadata: null;
  createdAt: Date;
  updatedAt: Date;
}

export interface Formats {
  thumbnail: Large;
  large: Large;
  medium: Large;
  small: Large;
}

export interface Large {
  name: string;
  hash: string;
  ext: string;
  mime: string;
  path: null;
  width: number;
  height: number;
  size: number;
  url: string;
}

export interface Meta {
  pagination: Pagination;
}

export interface Pagination {
  page: number;
  pageSize: number;
  pageCount: number;
  total: number;
}
