/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          500: "#13AC77",
          "accent-1": "#393939",
          "accent-2": "#0D011E",
        },
      },
      screens: {
        sm: "640px",
        // => @media (min-width: 640px) { ... }

        md: "768px",
        // => @media (min-width: 768px) { ... }

        lg: "1024px",
        // => @media (min-width: 1024px) { ... }

        xl: "1280px",
        // => @media (min-width: 1280px) { ... }
        // "2xl": "1400px",
        "2xl": "1536px",
        // => @media (min-width: 1536px) { ... }
      },

      fontFamily: {
        lexend: ["Lexend", "sans-serif"],
        lato: ["Lato", "sans-serif"],
      },

      backgroundImage: {
        "gradient-primary": "linear-gradient(141deg, #FFF 0%, #E4F9FF 100%)",
        "gradient-primary-2":
          "linear-gradient(49deg, rgba(19, 172, 119, 0.60) 0%, rgba(89, 194, 208, 0.60) 51.56%, rgba(172, 166, 19, 0.60) 100%)",
        "gradient-primary-3":
          "linear-gradient(49deg, rgba(19, 172, 119, 0.10) 0%, rgba(89, 194, 208, 0.10) 51.56%, rgba(172, 166, 19, 0.10) 100%)",
      },
      borderWidth: {
        DEFAULT: "1px",
        0: "0",
        2: "2px",
        3: "3px",
        4: "4px",
        6: "6px",
        8: "8px",
      },
    },
  },
  plugins: [],
};
